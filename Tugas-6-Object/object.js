// soal no.1
function arrayToObject(arr) {
    for (var i = 0; i < arr.length; i++){

        var person = arr[i][0].concat(" " + arr[i][1]);
        var person = (i + 1) + ". " + person;

        var object = {
          firstName: arr[i][0],
          lastName: arr[i][1],
          gender: arr[i][2],
          age: arr[i][3]
        } 

        if (object.age == undefined | object.age >= 2022 ) {
          object.age = "Invalid Birth Year";
        }

        var objectFull = {
          [person]: object
        }

        console.log(objectFull);
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
  
// Error case 
arrayToObject([]) // "{}"

console.log("\n");

//soal no.2
function shoppingTime(memberId, money) {
  var priceList = [1500000, 500000, 250000, 175000, 50000];
  var product = ["Sepatu Stacattu", "Baju Zoro", "Baju H&N", "Sweater Uniklooh", "Casing Handphone"];

  var peopleWishlist = {
    memberId : memberId,
    money : money,
    listPurchased: [],
    changeMoney: ''
  }

  if (memberId == undefined | memberId == '') {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else if (money < 50000){
    return "Mohon maaf, uang tidak cukup";
  } else if (money >= 50000){
    var i = 0
    while (i < 5){

      if (money >= 1500000){
        peopleWishlist.listPurchased.push(product[0]);
        selisih = money - priceList[0];
        money = selisih;
        i++;
      } 
      
      if (money >= 500000 & money < 1500000) {
        peopleWishlist.listPurchased.push(product[1]);
        selisih = money - priceList[1];
        money = selisih;
        i++;
      } 
      
      if (money >= 250000 & money < 500000) {
        peopleWishlist.listPurchased.push(product[2]);
        selisih = money - priceList[2];
        money = selisih;
        i++;
      }
      
      if (money >= 175000 & money < 250000) {
        peopleWishlist.listPurchased.push(product[3]);
        selisih = money - priceList[3];
        money = selisih;
        i++;
      }
      
      if (money >= 50000 & money < 175000) {
        peopleWishlist.listPurchased.push(product[4]);
        selisih = money - priceList[4];
        money = selisih;
        i++;
      } 
      

      if (selisih < 50000){
        peopleWishlist.changeMoney = money;
        break;
      }
    }
    return peopleWishlist;
  }
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log("\n");
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log("\n");
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log("\n");
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log("\n");
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("\n");
//soal no.3
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];

  list = []
  for (var i = 0; i < arrPenumpang.length ; i++) {
    var object = {
      penumpang: arrPenumpang[i][0],
      naikDari: arrPenumpang[i][1],
      tujuan: arrPenumpang[i][2],
      bayar: 2000 * (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1]))
    }

    list.push(object);
  }
  return list;
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));