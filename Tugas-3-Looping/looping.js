//soal no.1
console.log("LOOPING PERTAMA");
var angka = 2;

while(angka <= 20) {
    console.log(angka, "- I love coding");
    angka += 2
}

console.log("LOOPING KEDUA");
while(angka >= 2) {
    if (angka == 22) {
        angka -= 2
    }
    console.log(angka, "- I love coding");
    angka -= 2
}

//soal no.2
console.log("\n")
for(var number = 1; number <= 20 ; number++ ){
    if (number % 2 == 1 & number % 3 != 0){
        console.log(number, "- Santai");
    } else if (number % 2 == 0){
        console.log(number, "- Berkualitas");
    } else if (number % 2 == 1 & number % 3 == 0){
        console.log(number, "- I Love Coding");
    } else {
        console.log("error");
    }
}

console.log("\n");
//soal no.3
baris = 1
while(baris <= 4){
    console.log("########")
    baris++
}

console.log("\n");
//soal no.4
let string = "";
for(var baris = 1; baris <= 7; baris++ ){
    for(var kolom = 1; kolom <= baris; kolom++){
        string += "#";
    }
    string += "\n";
}
console.log(string);

console.log("\n")
//soal no.5
for(var check = 2; check <= 9; check++ ){
    if (check % 2 == 0){
        console.log(" # # # #");
    } else if (check % 2 == 1) {
        console.log("# # # # ");
    } else {
        console.log("error");
    }
}