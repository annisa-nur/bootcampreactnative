//soal no.1
function teriak(){
    return "Halo, Sanbers!"
}

console.log(teriak());

console.log("\n");

//soal no.2
function kalikan(num1,num2) {
    return num1 * num2
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1,num2)
console.log(hasilKali);

console.log("\n");

//soal no.3
function introduce(nama, age, address, hobby) {
    return ("Nama saya " + nama + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!");
}


var nama = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

var perkenalan = introduce(nama, age, address, hobby);
console.log(perkenalan);