//soal no.1
var nama = "John";
var peran = "";

console.log("Peran yang tersedia : ")
console.log("1. Penyihir");
console.log("2. Werewolf");
console.log("3. Guard");
console.log("\n");

if (nama == "" & peran == "") {
    console.log("Nama harus diisi!");
} else if (nama != "" & peran == "") {
    console.log("Halo", nama + "," , "Pilih peranmu untuk memulai game!");
} else if (nama != "" & peran == "Penyihir") {
    console.log("Selamat datang di Dunia Werewolf,", nama.replace(/,/, ' ').trim());
    console.log("Halo Penyihir", nama + ",", "kamu dapat melihat siapa yang menjadi werewolf!");
} else if (nama != "" & peran == "Guard") {
    console.log("Selamat datang di Dunia Werewolf,", nama.replace(/,/, ' ').trim());
    console.log("Halo Guard", nama + ",", "kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama != "" & peran == "Werewolf") {
    console.log("Selamat datang di Dunia Werewolf,", nama.replace(/,/, ' ').trim());
    console.log("Halo Werewolf", nama + ",", "Kamu akan memakan mangsa setiap malam!")
} else {
    console.log("Peran yang kamu masukkan tidak tersedia! Perhatikan penggunaan huruf kapital (case sensitive)")
}

console.log("\n");

//soal no.2
var tanggal = 21;
var bulan = 1;
var tahun = 1945;

if (tanggal >= 1 & tanggal <= 31 & bulan >= 1 & bulan <= 12 & tahun >= 1900 & tahun <= 2200) {
    switch(bulan) {
        case 1: { console.log(tanggal, "Januari", tahun); break; }
        case 2: { console.log(tanggal, "Februari", tahun); break; }
        case 3: { console.log(tanggal, "Maret", tahun); break; }
        case 4: { console.log(tanggal, "April", tahun); break; }
        case 5: { console.log(tanggal, "Mei", tahun); break; }
        case 6: { console.log(tanggal, "Juni", tahun); break; }
        case 7: { console.log(tanggal, "Juli", tahun); break; }
        case 8: { console.log(tanggal, "Agustus", tahun); break; }
        case 9: { console.log(tanggal, "September", tahun); break; }
        case 10: { console.log(tanggal, "Oktober", tahun); break; }
        case 11: { console.log(tanggal, "November", tahun); break; }
        case 12: { console.log(tanggal, "Desember", tahun); break; }
        default: { console.log(" "); break; }
    }
} else {
    console.log("Tanggal yang anda masukkan tidak tepat.")
}