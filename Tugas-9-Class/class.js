//soal no.1


class Animal {
    constructor(name, legs = 4, cold_blooded = false) {
        this.name = name;
        this.legs = legs;
        this.cold_blooded = cold_blooded;
    }
    get cnam(){
        return this.name, this.legs, this.cold_blooded;
    }
    set cnam(x){
        this.name = x
        this.legs = x
        this.cold_blooded = x
    }
}
 
const sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Code class Ape dan class Frog di sini  
class Frog extends Animal {
    constructor(name, legs = 4, cold_blooded = true, jump) {
        super(name); 
        this.cold_blooded = cold_blooded 
        this.jump = function() {
            console.log("hop hop");
          };
    }
}

class Ape extends Animal {
    constructor(name, legs = 2, cold_blooded = false, yell) {
        super(name);
        this.legs = legs
        this.yell = function() {
            console.log("Auooo");
          };
        
    }
}

const sungokong = new Ape("kera sakti");
console.log(sungokong.name); // "kera sakti"
console.log(sungokong.legs); // 2
console.log(sungokong.cold_blooded); // false
sungokong.yell(); // "Auooo"

const kodok = new Frog("buduk");
console.log(kodok.name); // "buduk"
console.log(kodok.legs); // 4
console.log(kodok.cold_blooded); // true
kodok.jump(); // "hop hop"

console.log("\n");

//soal no.2
class Clock {
  constructor({template}){
      this.template = template;
  }
  render(){
      var date = new Date ();

      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;

      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
      
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;

      var output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);  

    console.log(output);
  }
  stop(){
      clearInterval(this.timer);
  }

  start(){
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
  }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 

