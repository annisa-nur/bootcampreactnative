const readBooks = require('./callback.js')

const books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

let waktu = 10000
let i = 0

function callbek () {
    readBooks(waktu, books[i], function(sisaWaktu) {
        waktu = sisaWaktu
        i++
        if (i < books.length)
        callbek()
    })
}

callbek();