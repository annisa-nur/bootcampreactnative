const readBooksPromise = require('./promise.js')
 
const books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

async function janji(){
    let waktu = 10000
    for (var i = 0; i < books.length ; i++){
        waktu = await readBooksPromise(waktu, books[i])
        .then(function(sisaWaktu){
            return sisaWaktu;
        })
        .catch(function (sisaWaktu){
            return sisaWaktu;
        })
    }
}

janji();