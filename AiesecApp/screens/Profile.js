import React from 'react';
import { StyleSheet, ScrollView, Image, Text, View, TextInput, TouchableOpacity  } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { images } from '../constants';

export default function Profile() {
    return (
        <>
            <ScrollView>
            <View style={StyleSheet.container}>
                <View style={styles.header}>
                </View>
                    <View style={styles.container3}>
                        <Image 
                            source={images.profile}
                        />
                    </View>
            </View>
            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    container3:{
        alignItems: 'center',
        justifyContent: 'center',
    }, 
    header: {
        flexDirection: "row",
        backgroundColor: 'white',
        paddingVertical: 2,
        paddingHorizontal: 2,
        alignItems: 'center',
    },

})