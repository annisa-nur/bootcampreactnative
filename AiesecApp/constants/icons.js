const back = require("../assets/icons/back.png");
const bell = require("../assets/icons/bell.png");
const bill = require("../assets/icons/bill.png");
const close = require("../assets/icons/close.png");
const disable_eye = require("../assets/icons/disable_eye.png");
const down = require("../assets/icons/down.png");
const eye = require("../assets/icons/eye.png")
const notes = require("../assets/icons/notes.png");
const barcode = require("../assets/icons/barcode.png");
const info = require("../assets/icons/info.png");
const spaces = require("../assets/icons/spaces.png");
const more = require("../assets/icons/more.png");
const database = require("../assets/icons/database.png");
const phone = require("../assets/icons/phone.png");
const goals = require("../assets/icons/goals.png");
const scan = require("../assets/icons/scan.png");
const time = require("../assets/icons/time.png");
const user = require("../assets/icons/user.png");
const timeline = require("../assets/icons/timeline.png");

export default {
    back,
    bell,
    bill,
    close,
    disable_eye,
    down,
    eye,
    notes,
    barcode,
    info,
    spaces,
    more,
    database,
    phone,
    goals,
    scan,
    time,
    user,
    timeline
}