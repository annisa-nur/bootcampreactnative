const Logo = require("../assets/images/logo.png");
const banner = require("../assets/images/banner.png");
const newsBanner = require("../assets/images/newsBanner.png");
const focus = require("../assets/images/focus.png");
const Logo2 = require("../assets/images/logo2.png");
const profile = require("../assets/images/profile.png");
// Dummy
const indoFlag = require("../assets/images/indo-flag.jpg");


export default {
    Logo,
    banner,
    newsBanner,
    focus,
    profile,
    Logo2,
    // Dummy
    indoFlag
}