import React, { useEffect } from 'react'
import { useState } from 'react'
import { StyleSheet, Text, View, Image, Button } from 'react-native'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { Data }from './data'

const numColumns = 2;
export default function Home({route, navigation}) {
    const { username } = route.params;
    const [totalPrice, setTotalPrice] = useState(0);

    const formatData = (data, numColumns) => {
        const numberOfFullRows = Math.floor(data.length / numColumns);

        let numberOfElementLastRow = data.length - (numberOfFullRows * numColumns)
        while (numberOfElementLastRow !== numColumns && numberOfElementLastRow !== 0) {
            data.push({ key: `blank-${numberOfElementLastRow}`, empty: true});
            numberOfElementLastRow = numberOfElementLastRow + 1;
        }
        return data;
    }

    // const currencyFormat=(num)=> {
    //     return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    //   };

    const updateHarga =(price)=>{
        // console.log("UpdatPrice : " + price);
        // const temp = Number(price) + totalPrice;
        // console.log(temp)
        // setTotalPrice(temp)
        
        //? #Bonus (10 poin) -- HomeScreen.js --
        //? agar harga dapat update misal di tambah lebih dari 1 item atau lebih -->
            
    }
    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16}}>
                <View>
                    <Text>Selamat Datang,</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>
            <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 60}}>
            {
                <FlatList
                    data={formatData(data, numColumns)}
                    renderItem={({ item }) => (
                        if (item.empty === true) {
                            return <View style={[styles.item, styles.itemInvisible]} />;
                        }
                            <TouchableOpacity style={style.satu}>

                            </TouchableOpacity>
                    )}
                />
             }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,        
        backgroundColor:'white', 
    },  
    content:{
        width: 150,
        height: 220,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',    
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    }
        
})


// mohon maaf saya masi belum bisa :'D