import React from 'react';
import { StyleSheet, ScrollView, Image, Text, View, TextInput, TouchableOpacity  } from 'react-native';
import { StatusBar } from 'expo-status-bar';

export default function AboutScreen() {
    return (
        <>
            <StatusBar
                style= "light"
                translucent={false}
                backgroundColor= '#CACCD1'
            />
            
            <View style={StyleSheet.container}>
                <View style={styles.header}>
                    <Text style={styles.headerTitle}>ABOUT DEVELOPER</Text>
                    <Image
                        style={styles.headerIcon}
                        source={require('./asset/kanan.png')}
                    />
                </View>
                <View style={styles.container2}>
                    <Image 
                        style={styles.photo}
                        source={require('./asset/photoprof.png')}
                    />
                    <Text style={styles.textName}>Annisa Nur</Text>
                </View>
                <View style={styles.container3}>
                    <Image 
                        style={styles.sosmed}
                        source={require('./asset/Group 6.png')}
                    />
                    <Image 
                        style={styles.porto}
                        source={require('./asset/Group 7.png')}
                    />
                    <Image
                        style={styles.Other}
                        source={require('./asset/Group 8.png')}
                    />
                </View>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    container2: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 450
    },
    container3:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 20,
        paddingHorizontal: 20
    }, 
    header: {
        flexDirection: "row",
        backgroundColor: '#037EF2',
        paddingVertical: 15,
        paddingHorizontal: 15,
        alignItems: 'center',
    },
    headerIcon: {
        height: 25,
        width: 25,
        resizeMode: 'contain',
    },
    headerTitle:{
        flex: 1,
        fontSize: 13,
        fontWeight: 'bold',
        color: 'white',
        paddingHorizontal: 20,
    },
    photo: {
        width: 123,
        height: 113,
        resizeMode:'contain'
    },
    textName: {
        flex: 1,
        fontFamily: 'lato',
        fontWeight: 'bold',
        fontSize: 28,
        paddingHorizontal: 20,
    },
    sosmed: {
        width: 327,
        height: 92,
    },
    porto: {
        marginTop: 10,
        width: 327, 
        height: 92
    },
    Other: {
        marginTop : 10,
        width: 328,
        height: 254,
    }

})