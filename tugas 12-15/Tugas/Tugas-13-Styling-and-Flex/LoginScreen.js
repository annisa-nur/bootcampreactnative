import React from 'react';
import { StyleSheet, Image, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { StatusBar } from 'expo-status-bar';

export default class App extends React.Component {
  state={
    email:"",
    password:""
  }
  render(){
    return (
      <>
        <StatusBar
          style= "light"
          translucent={false}
          backgroundColor= "#CACCD1"
        />
      
        <View style={styles.container2}>
          <Image
            style={styles.logo}
            source={require("./asset/logo.png")}
          />
          <View style={styles.container3}>
            <Text style={styles.titlepage}>LOGIN</Text>
          </View>
          <View style={styles.container}>
            <View style={styles.inputView} >
              <TextInput  
                style={styles.inputText}
                placeholder="Email..." 
                placeholderTextColor="#003f5c"
                onChangeText={text => this.setState({email:text})}/>
            </View>
            <View style={styles.inputView} >
              <TextInput  
                secureTextEntry
                style={styles.inputText}
                placeholder="Password..." 
                placeholderTextColor="#003f5c"
                onChangeText={text => this.setState({password:text})}/>
            </View>
            <TouchableOpacity>
              <Text style={styles.forgot}>Forgot Password?</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.loginBtn}>
              <Text style={styles.loginText}>LOGIN</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.SignUpBtn}>
              <Text style={styles.signUpText}>Sign Up</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={styles.loginText}>Signup</Text>
            </TouchableOpacity>
          </View>
  
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container2: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: 15,
    paddingTop: 15,
  },
  container3: {
    marginTop: 60,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo:{
    width: 296,
    height: 44,
  },
  titlepage:{
    fontFamily: "lato",
    fontWeight:"bold",
    fontSize:50,
    color:"#000000",
    marginBottom:40
  },
  inputView:{
    width:"80%",
    backgroundColor:"#EFEFEF",
    borderRadius:25,
    height:50,
    marginBottom:20,
    justifyContent:"center",
    padding:20
  },
  inputText:{
    fontFamily: "lato",
    height:50,
    color:"#000000"
  },
  forgot:{
    fontFamily: "lato",
    color:"#000000",
    fontSize:11,
  },
  loginBtn:{
    width:"80%",
    backgroundColor:"#037EF2",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10
  },
  SignUpBtn:{
    width:"80%",
    backgroundColor:"#EFEFEF",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:5,
    marginBottom:10
  },
  loginText:{
    fontFamily: "lato",
    color:"white"
  },
  signUpText:{
    fontFamily: "lato",
    color:"#000000"
  }
});