//soal no.13 a
var sentence = "Pada Hari Minggu ku turut ayah ke kota";

function replaceCharacter(sentence, targetChar, charChange){
    if (charChange == undefined) {
        sentence = sentence.split(targetChar);
        sentence = sentence.join('');
        return sentence;
    } else if (targetChar != undefined){
        sentence = sentence.split(targetChar);
        sentence = sentence.join(charChange);
        return sentence;
    } else {
        console.log("error");
    }
}

var result = replaceCharacter(sentence, "a", "o");
console.log(result);

console.log("\n");

//soal no.13 b
// Format Data [absen, nama, nilai]
var studentData = [
	[2, "John Duro", 60],
	[4, "Robin Ackerman", 100],
	[1, "Jaeger Marimo", 60],
	[6, "Zoro", 80],
	[5, "Zenitsu", 80],
	[3, "Patrick Zala", 90],
];

function sortGrade(data){
    data.sort(function(a,b) { return a[0] - b[0]});
    data.sort(function(a,b) { return b[2] - a[2]});
    return data;
}

var sortedData = sortGrade(studentData);
console.log(sortedData);