// //soal no.1
function range(startNum, finishNum) {
    if (startNum == undefined | finishNum == undefined){
        return [-1];
    } else if (startNum > finishNum) {
        array2 = [];
        for (var number = startNum; number >= finishNum; number-- ){
            array2.push(number);
        }
        return array2; 
    } else if (startNum < finishNum) {
        array3 = [];
        for (var number = startNum; number <= finishNum; number++ ){
            array3.push(number);
        }
        return array3;
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

console.log("\n");

//soal no.2
function rangeWithStep(startNum, finishNum, step) {
    if (startNum == undefined | finishNum == undefined | step == undefined){
        return [-1];
    } else if (startNum > finishNum) {
        array2 = [];
        for (var number = startNum; number >= finishNum; number -= step ){
            array2.push(number);
        }
        return array2; 
    } else if (startNum < finishNum) {
        array3 = [];
        for (var number = startNum; number <= finishNum; number += step ){
            array3.push(number);
        }
        return array3;
    }

}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log("\n");

//soal no.3
function sum(startNum, finishNum, step) {
    if (startNum == undefined){
        return 0;
    } else if (finishNum == undefined & step == undefined) {
        return startNum;
    } else if(startNum > finishNum & step == undefined) {
        let sum3 = 0;
        for (var number = startNum; number >= finishNum; number-- ){
            sum3 += number
        }
        return sum3; 
    } else if(startNum < finishNum & step == undefined) {
        let sum3 = 0;
        for (var number = startNum; number <= finishNum; number++ ){
            sum3 += number
        }
        return sum3; 
    } else if (startNum > finishNum & step != undefined) {
        let sum2 = 0;
        for (var number = startNum; number >= finishNum; number -= step ){
            sum2 += number
        }
        return sum2; 
    } else if (startNum < finishNum & step != undefined) {
        let sum1 = 0;
        for (var number = startNum; number <= finishNum; number += step ){
            sum1 += number;
        }
        return sum1;
    }

}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log("\n");
//soal no.4 
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling () {
    for (let i = 0; i < input.length; i++) {
        console.log("Nomor ID: " + input[i][0]);
        console.log("Nama Lengkap: " + input[i][1]);
        console.log("TTL: " + input[i][2] + " " + input[i][3] );
        console.log("Hobi: " + input[i][4]);

        console.log("\n");
    }
}

dataHandling();

console.log("\n");
//soal no.5
function balikKata(kata) {
    let balik = "";
    for (var i = kata.length - 1; i >= 0; i--) {
        balik += kata[i]
    }
    return balik;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

console.log("\n");
//soal no.6
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
var asal, jeniskel, sekolah;

function dataHandling2(asal,jeniskel,sekolah) {
    if (asal == undefined | jeniskel == undefined | sekolah == undefined){
        console.log("error");
    } else {
        //fungsi splice
        input.splice(2,1,asal);
        input.splice(4,1,jeniskel)
        input.splice(5,0,sekolah);
    }
}

//main()
dataHandling2("Provinsi Bandar Lampung", "Pria", "SMA Internasional Metro");
console.log(input);

//fungsi split
tanggal = input[3]
tanggal = tanggal.split("/");
bulan = Number(tanggal[1]);
switch(bulan) {
    case 1: {console.log("Januari"); break; }
    case 2: {console.log("Februari"); break; }
    case 3: {console.log("Maret"); break; }
    case 4: {console.log("April"); break; }
    case 5: {console.log("Mei"); break; }
    case 6: {console.log("Juni"); break; }
    case 7: {console.log("Juli"); break; }
    case 8: {console.log("Agustus"); break; }
    case 9: {console.log("September"); break; }
    case 10: {console.log("oktober"); break; }
    case 11: {console.log("November"); break; }
    case 12: {console.log("Desember"); break; }
}


//fungsi sort 
var tanggalsort = tanggal.sort(function (value1, value2) { return value2 - value1});
console.log(tanggalsort);

//fungsi join
var tanggalsortascend = tanggal.sort(function (value1, value2) { return value1 - value2});
var tanggaljoin = tanggalsortascend.join("-");
console.log(tanggaljoin);

//fungsi slice
nama = input[1];
nama = nama.slice(0,16);
console.log(nama);
